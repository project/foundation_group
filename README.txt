Foundation Group
================

This module adds a new set of group styles to the Field Group options when configuring a view mode display.
Simply enable the module, and nest Fields from "Manage Display" like so:

Main Container (Foundation Accordion container)
 - Group Container 1 (Foundation Accordion item)
   - Field 1
   - Field 2
 - Group Container 2 (Foundation Accordion item)
   - Field 3
   - Field 4

This implements Zurb Foundation 5.x's Accordion and Tab component. Please refer to the Foundation 5.x
documentation for more information.

TODO: SUPPORT tab options